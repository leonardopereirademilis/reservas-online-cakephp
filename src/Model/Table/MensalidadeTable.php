<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Mensalidade Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Condominio
 * @property \Cake\ORM\Association\BelongsTo $Plano
 * @property \Cake\ORM\Association\HasMany $Boleto
 *
 * @method \App\Model\Entity\Mensalidade get($primaryKey, $options = [])
 * @method \App\Model\Entity\Mensalidade newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Mensalidade[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Mensalidade|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Mensalidade patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Mensalidade[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Mensalidade findOrCreate($search, callable $callback = null)
 */
class MensalidadeTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('mensalidade');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Condominio', [
            'foreignKey' => 'condominio_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Plano', [
            'foreignKey' => 'plano_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Boleto', [
            'foreignKey' => 'mensalidade_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->integer('ano')
            ->requirePresence('ano', 'create')
            ->notEmpty('ano');

        $validator
            ->integer('mes')
            ->requirePresence('mes', 'create')
            ->notEmpty('mes');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['condominio_id'], 'Condominio'));
        $rules->add($rules->existsIn(['plano_id'], 'Plano'));

        return $rules;
    }
}
