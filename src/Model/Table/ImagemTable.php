<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Imagem Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Recurso
 *
 * @method \App\Model\Entity\Imagem get($primaryKey, $options = [])
 * @method \App\Model\Entity\Imagem newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Imagem[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Imagem|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Imagem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Imagem[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Imagem findOrCreate($search, callable $callback = null)
 */
class ImagemTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('imagem');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Recurso', [
            'foreignKey' => 'recurso_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('imagem');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['recurso_id'], 'Recurso'));

        return $rules;
    }
}
