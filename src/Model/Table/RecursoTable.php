<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Recurso Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Condominio
 * @property \Cake\ORM\Association\BelongsTo $UnidadeTempoReserva
 * @property \Cake\ORM\Association\BelongsTo $Cor
 * @property \Cake\ORM\Association\HasMany $Imagem
 * @property \Cake\ORM\Association\HasMany $Indisponibilidade
 * @property \Cake\ORM\Association\HasMany $Reserva
 * @property \Cake\ORM\Association\BelongsToMany $TipoReserva
 *
 * @method \App\Model\Entity\Recurso get($primaryKey, $options = [])
 * @method \App\Model\Entity\Recurso newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Recurso[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Recurso|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Recurso patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Recurso[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Recurso findOrCreate($search, callable $callback = null)
 */
class RecursoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('recurso');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Condominio', [
            'foreignKey' => 'condominio_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('UnidadeTempoReserva', [
            'foreignKey' => 'unidade_tempo_reserva_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Cor', [
            'foreignKey' => 'cor_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Imagem', [
            'foreignKey' => 'recurso_id'
        ]);
        $this->hasMany('Indisponibilidade', [
            'foreignKey' => 'recurso_id'
        ]);
        $this->hasMany('Reserva', [
            'foreignKey' => 'recurso_id'
        ]);
        $this->belongsToMany('TipoReserva', [
            'foreignKey' => 'recurso_id',
            'targetForeignKey' => 'tipo_reserva_id',
            'joinTable' => 'recurso_tipo_reserva'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('ativo', 'create')
            ->notEmpty('ativo');

        $validator
            ->integer('capacidade')
            ->allowEmpty('capacidade');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->requirePresence('exige_confirmacao', 'create')
            ->notEmpty('exige_confirmacao');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->integer('numero_max_reservas')
            ->requirePresence('numero_max_reservas', 'create')
            ->notEmpty('numero_max_reservas');

        $validator
            ->integer('tempo_reserva')
            ->requirePresence('tempo_reserva', 'create')
            ->notEmpty('tempo_reserva');

        $validator
            ->decimal('valor')
            ->allowEmpty('valor');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['condominio_id'], 'Condominio'));
        $rules->add($rules->existsIn(['unidade_tempo_reserva_id'], 'UnidadeTempoReserva'));
        $rules->add($rules->existsIn(['cor_id'], 'Cor'));

        return $rules;
    }
}
