<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Convidado Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Reserva
 *
 * @method \App\Model\Entity\Convidado get($primaryKey, $options = [])
 * @method \App\Model\Entity\Convidado newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Convidado[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Convidado|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Convidado patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Convidado[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Convidado findOrCreate($search, callable $callback = null)
 */
class ConvidadoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('convidado');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Reserva', [
            'foreignKey' => 'reserva_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('cpf', 'create')
            ->notEmpty('cpf');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->requirePresence('telefone', 'create')
            ->notEmpty('telefone');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['reserva_id'], 'Reserva'));

        return $rules;
    }
}
