<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Boleto Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Mensalidade
 *
 * @method \App\Model\Entity\Boleto get($primaryKey, $options = [])
 * @method \App\Model\Entity\Boleto newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Boleto[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Boleto|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Boleto patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Boleto[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Boleto findOrCreate($search, callable $callback = null)
 */
class BoletoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('boleto');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Mensalidade', [
            'foreignKey' => 'mensalidade_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('codigo', 'create')
            ->notEmpty('codigo');

        $validator
            ->dateTime('data_geracao')
            ->requirePresence('data_geracao', 'create')
            ->notEmpty('data_geracao');

        $validator
            ->dateTime('data_pagamento')
            ->allowEmpty('data_pagamento');

        $validator
            ->dateTime('data_vencimento')
            ->requirePresence('data_vencimento', 'create')
            ->notEmpty('data_vencimento');

        $validator
            ->requirePresence('pago', 'create')
            ->notEmpty('pago');

        $validator
            ->decimal('valor')
            ->requirePresence('valor', 'create')
            ->notEmpty('valor');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['mensalidade_id'], 'Mensalidade'));

        return $rules;
    }
}
