<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Apartamento Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Condominio
 * @property \Cake\ORM\Association\HasMany $Convite
 * @property \Cake\ORM\Association\HasMany $Reserva
 *
 * @method \App\Model\Entity\Apartamento get($primaryKey, $options = [])
 * @method \App\Model\Entity\Apartamento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Apartamento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Apartamento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Apartamento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Apartamento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Apartamento findOrCreate($search, callable $callback = null)
 */
class ApartamentoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('apartamento');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Condominio', [
            'foreignKey' => 'condominio_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Convite', [
            'foreignKey' => 'apartamento_id'
        ]);
        $this->hasMany('Reserva', [
            'foreignKey' => 'apartamento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('bloco');

        $validator
            ->integer('numero')
            ->requirePresence('numero', 'create')
            ->notEmpty('numero');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['condominio_id'], 'Condominio'));

        return $rules;
    }
}
