<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TipoReserva Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Recurso
 *
 * @method \App\Model\Entity\TipoReserva get($primaryKey, $options = [])
 * @method \App\Model\Entity\TipoReserva newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TipoReserva[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TipoReserva|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TipoReserva patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TipoReserva[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TipoReserva findOrCreate($search, callable $callback = null)
 */
class TipoReservaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tipo_reserva');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsToMany('Recurso', [
            'foreignKey' => 'tipo_reserva_id',
            'targetForeignKey' => 'recurso_id',
            'joinTable' => 'recurso_tipo_reserva'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }
}
