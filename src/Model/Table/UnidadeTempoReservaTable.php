<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UnidadeTempoReserva Model
 *
 * @property \Cake\ORM\Association\HasMany $Recurso
 *
 * @method \App\Model\Entity\UnidadeTempoReserva get($primaryKey, $options = [])
 * @method \App\Model\Entity\UnidadeTempoReserva newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UnidadeTempoReserva[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UnidadeTempoReserva|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UnidadeTempoReserva patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UnidadeTempoReserva[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UnidadeTempoReserva findOrCreate($search, callable $callback = null)
 */
class UnidadeTempoReservaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('unidade_tempo_reserva');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Recurso', [
            'foreignKey' => 'unidade_tempo_reserva_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        return $validator;
    }
}
