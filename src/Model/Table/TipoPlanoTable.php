<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TipoPlano Model
 *
 * @property \Cake\ORM\Association\HasMany $Plano
 *
 * @method \App\Model\Entity\TipoPlano get($primaryKey, $options = [])
 * @method \App\Model\Entity\TipoPlano newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TipoPlano[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TipoPlano|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TipoPlano patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TipoPlano[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TipoPlano findOrCreate($search, callable $callback = null)
 */
class TipoPlanoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tipo_plano');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Plano', [
            'foreignKey' => 'tipo_plano_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('ativo', 'create')
            ->notEmpty('ativo');

        $validator
            ->dateTime('data_criacao')
            ->requirePresence('data_criacao', 'create')
            ->notEmpty('data_criacao');

        $validator
            ->dateTime('data_encerramento')
            ->allowEmpty('data_encerramento');

        $validator
            ->integer('nu_max_apartamentos')
            ->requirePresence('nu_max_apartamentos', 'create')
            ->notEmpty('nu_max_apartamentos');

        $validator
            ->integer('nu_max_recursos')
            ->requirePresence('nu_max_recursos', 'create')
            ->notEmpty('nu_max_recursos');

        $validator
            ->decimal('valor')
            ->requirePresence('valor', 'create')
            ->notEmpty('valor');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        return $validator;
    }
}
