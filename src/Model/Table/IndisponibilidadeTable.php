<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Indisponibilidade Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Recurso
 *
 * @method \App\Model\Entity\Indisponibilidade get($primaryKey, $options = [])
 * @method \App\Model\Entity\Indisponibilidade newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Indisponibilidade[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Indisponibilidade|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Indisponibilidade patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Indisponibilidade[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Indisponibilidade findOrCreate($search, callable $callback = null)
 */
class IndisponibilidadeTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('indisponibilidade');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Recurso', [
            'foreignKey' => 'recurso_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('ativo', 'create')
            ->notEmpty('ativo');

        $validator
            ->dateTime('data_inicio')
            ->requirePresence('data_inicio', 'create')
            ->notEmpty('data_inicio');

        $validator
            ->dateTime('data_fim')
            ->requirePresence('data_fim', 'create')
            ->notEmpty('data_fim');

        $validator
            ->requirePresence('motivo', 'create')
            ->notEmpty('motivo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['recurso_id'], 'Recurso'));

        return $rules;
    }
}
