<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Boleto Entity
 *
 * @property int $id
 * @property string $codigo
 * @property \Cake\I18n\Time $data_geracao
 * @property \Cake\I18n\Time $data_pagamento
 * @property \Cake\I18n\Time $data_vencimento
 * @property int $mensalidade_id
 * @property string $pago
 * @property float $valor
 *
 * @property \App\Model\Entity\Mensalidade $mensalidade
 */
class Boleto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
