<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Recurso Entity
 *
 * @property int $id
 * @property string $ativo
 * @property int $capacidade
 * @property int $condominio_id
 * @property string $descricao
 * @property string $exige_confirmacao
 * @property string $nome
 * @property int $numero_max_reservas
 * @property int $tempo_reserva
 * @property float $valor
 * @property int $unidade_tempo_reserva_id
 * @property int $cor_id
 *
 * @property \App\Model\Entity\Condominio $condominio
 * @property \App\Model\Entity\UnidadeTempoReserva $unidade_tempo_reserva
 * @property \App\Model\Entity\Cor $cor
 * @property \App\Model\Entity\Imagem[] $imagem
 * @property \App\Model\Entity\Indisponibilidade[] $indisponibilidade
 * @property \App\Model\Entity\Reserva[] $reserva
 * @property \App\Model\Entity\TipoReserva[] $tipo_reserva
 */
class Recurso extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
