<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TipoPlano Entity
 *
 * @property int $id
 * @property string $ativo
 * @property \Cake\I18n\Time $data_criacao
 * @property \Cake\I18n\Time $data_encerramento
 * @property int $nu_max_apartamentos
 * @property int $nu_max_recursos
 * @property float $valor
 * @property string $descricao
 *
 * @property \App\Model\Entity\Plano[] $plano
 */
class TipoPlano extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
