<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Mensalidade Entity
 *
 * @property int $id
 * @property int $ano
 * @property int $condominio_id
 * @property int $mes
 * @property int $plano_id
 *
 * @property \App\Model\Entity\Condominio $condominio
 * @property \App\Model\Entity\Plano $plano
 * @property \App\Model\Entity\Boleto[] $boleto
 */
class Mensalidade extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
