<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Apartamento Entity
 *
 * @property int $id
 * @property string $bloco
 * @property int $condominio_id
 * @property int $numero
 *
 * @property \App\Model\Entity\Condominio $condominio
 * @property \App\Model\Entity\Convite[] $convite
 * @property \App\Model\Entity\Reserva[] $reserva
 */
class Apartamento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
