<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Convidado Controller
 *
 * @property \App\Model\Table\ConvidadoTable $Convidado
 */
class ConvidadoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Reserva']
        ];
        $convidado = $this->paginate($this->Convidado);

        $this->set(compact('convidado'));
        $this->set('_serialize', ['convidado']);
    }

    /**
     * View method
     *
     * @param string|null $id Convidado id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $convidado = $this->Convidado->get($id, [
            'contain' => ['Reserva']
        ]);

        $this->set('convidado', $convidado);
        $this->set('_serialize', ['convidado']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $convidado = $this->Convidado->newEntity();
        if ($this->request->is('post')) {
            $convidado = $this->Convidado->patchEntity($convidado, $this->request->data);
            if ($this->Convidado->save($convidado)) {
                $this->Flash->success(__('The convidado has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The convidado could not be saved. Please, try again.'));
            }
        }
        $reserva = $this->Convidado->Reserva->find('list', ['limit' => 200]);
        $this->set(compact('convidado', 'reserva'));
        $this->set('_serialize', ['convidado']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Convidado id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $convidado = $this->Convidado->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $convidado = $this->Convidado->patchEntity($convidado, $this->request->data);
            if ($this->Convidado->save($convidado)) {
                $this->Flash->success(__('The convidado has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The convidado could not be saved. Please, try again.'));
            }
        }
        $reserva = $this->Convidado->Reserva->find('list', ['limit' => 200]);
        $this->set(compact('convidado', 'reserva'));
        $this->set('_serialize', ['convidado']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Convidado id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $convidado = $this->Convidado->get($id);
        if ($this->Convidado->delete($convidado)) {
            $this->Flash->success(__('The convidado has been deleted.'));
        } else {
            $this->Flash->error(__('The convidado could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
