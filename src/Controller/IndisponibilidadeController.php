<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Indisponibilidade Controller
 *
 * @property \App\Model\Table\IndisponibilidadeTable $Indisponibilidade
 */
class IndisponibilidadeController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Recurso']
        ];
        $indisponibilidade = $this->paginate($this->Indisponibilidade);

        $this->set(compact('indisponibilidade'));
        $this->set('_serialize', ['indisponibilidade']);
    }

    /**
     * View method
     *
     * @param string|null $id Indisponibilidade id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $indisponibilidade = $this->Indisponibilidade->get($id, [
            'contain' => ['Recurso']
        ]);

        $this->set('indisponibilidade', $indisponibilidade);
        $this->set('_serialize', ['indisponibilidade']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $indisponibilidade = $this->Indisponibilidade->newEntity();
        if ($this->request->is('post')) {
            $indisponibilidade = $this->Indisponibilidade->patchEntity($indisponibilidade, $this->request->data);
            if ($this->Indisponibilidade->save($indisponibilidade)) {
                $this->Flash->success(__('The indisponibilidade has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The indisponibilidade could not be saved. Please, try again.'));
            }
        }
        $recurso = $this->Indisponibilidade->Recurso->find('list', ['limit' => 200]);
        $this->set(compact('indisponibilidade', 'recurso'));
        $this->set('_serialize', ['indisponibilidade']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Indisponibilidade id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $indisponibilidade = $this->Indisponibilidade->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $indisponibilidade = $this->Indisponibilidade->patchEntity($indisponibilidade, $this->request->data);
            if ($this->Indisponibilidade->save($indisponibilidade)) {
                $this->Flash->success(__('The indisponibilidade has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The indisponibilidade could not be saved. Please, try again.'));
            }
        }
        $recurso = $this->Indisponibilidade->Recurso->find('list', ['limit' => 200]);
        $this->set(compact('indisponibilidade', 'recurso'));
        $this->set('_serialize', ['indisponibilidade']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Indisponibilidade id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $indisponibilidade = $this->Indisponibilidade->get($id);
        if ($this->Indisponibilidade->delete($indisponibilidade)) {
            $this->Flash->success(__('The indisponibilidade has been deleted.'));
        } else {
            $this->Flash->error(__('The indisponibilidade could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
