<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Recurso Controller
 *
 * @property \App\Model\Table\RecursoTable $Recurso
 */
class RecursoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Condominio', 'UnidadeTempoReserva', 'Cor']
        ];
        $recurso = $this->paginate($this->Recurso);

        $this->set(compact('recurso'));
        $this->set('_serialize', ['recurso']);
    }

    /**
     * View method
     *
     * @param string|null $id Recurso id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $recurso = $this->Recurso->get($id, [
            'contain' => ['Condominio', 'UnidadeTempoReserva', 'Cor', 'TipoReserva', 'Imagem', 'Indisponibilidade', 'Reserva']
        ]);

        $this->set('recurso', $recurso);
        $this->set('_serialize', ['recurso']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $recurso = $this->Recurso->newEntity();
        if ($this->request->is('post')) {
            $recurso = $this->Recurso->patchEntity($recurso, $this->request->data);
            if ($this->Recurso->save($recurso)) {
                $this->Flash->success(__('The recurso has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The recurso could not be saved. Please, try again.'));
            }
        }
        $condominio = $this->Recurso->Condominio->find('list', ['limit' => 200]);
        $unidadeTempoReserva = $this->Recurso->UnidadeTempoReserva->find('list', ['limit' => 200]);
        $cor = $this->Recurso->Cor->find('list', ['limit' => 200]);
        $tipoReserva = $this->Recurso->TipoReserva->find('list', ['limit' => 200]);
        $this->set(compact('recurso', 'condominio', 'unidadeTempoReserva', 'cor', 'tipoReserva'));
        $this->set('_serialize', ['recurso']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Recurso id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $recurso = $this->Recurso->get($id, [
            'contain' => ['TipoReserva']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $recurso = $this->Recurso->patchEntity($recurso, $this->request->data);
            if ($this->Recurso->save($recurso)) {
                $this->Flash->success(__('The recurso has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The recurso could not be saved. Please, try again.'));
            }
        }
        $condominio = $this->Recurso->Condominio->find('list', ['limit' => 200]);
        $unidadeTempoReserva = $this->Recurso->UnidadeTempoReserva->find('list', ['limit' => 200]);
        $cor = $this->Recurso->Cor->find('list', ['limit' => 200]);
        $tipoReserva = $this->Recurso->TipoReserva->find('list', ['limit' => 200]);
        $this->set(compact('recurso', 'condominio', 'unidadeTempoReserva', 'cor', 'tipoReserva'));
        $this->set('_serialize', ['recurso']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Recurso id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $recurso = $this->Recurso->get($id);
        if ($this->Recurso->delete($recurso)) {
            $this->Flash->success(__('The recurso has been deleted.'));
        } else {
            $this->Flash->error(__('The recurso could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
