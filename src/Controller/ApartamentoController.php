<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Apartamento Controller
 *
 * @property \App\Model\Table\ApartamentoTable $Apartamento
 */
class ApartamentoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Condominio']
        ];
        $apartamento = $this->paginate($this->Apartamento);

        $this->set(compact('apartamento'));
        $this->set('_serialize', ['apartamento']);
    }

    /**
     * View method
     *
     * @param string|null $id Apartamento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $apartamento = $this->Apartamento->get($id, [
            'contain' => ['Condominio', 'Convite', 'Reserva']
        ]);

        $this->set('apartamento', $apartamento);
        $this->set('_serialize', ['apartamento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $apartamento = $this->Apartamento->newEntity();
        if ($this->request->is('post')) {
            $apartamento = $this->Apartamento->patchEntity($apartamento, $this->request->data);
            if ($this->Apartamento->save($apartamento)) {
                $this->Flash->success(__('The apartamento has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The apartamento could not be saved. Please, try again.'));
            }
        }
        $condominio = $this->Apartamento->Condominio->find('list', ['limit' => 200]);
        $this->set(compact('apartamento', 'condominio'));
        $this->set('_serialize', ['apartamento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Apartamento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $apartamento = $this->Apartamento->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $apartamento = $this->Apartamento->patchEntity($apartamento, $this->request->data);
            if ($this->Apartamento->save($apartamento)) {
                $this->Flash->success(__('The apartamento has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The apartamento could not be saved. Please, try again.'));
            }
        }
        $condominio = $this->Apartamento->Condominio->find('list', ['limit' => 200]);
        $this->set(compact('apartamento', 'condominio'));
        $this->set('_serialize', ['apartamento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Apartamento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $apartamento = $this->Apartamento->get($id);
        if ($this->Apartamento->delete($apartamento)) {
            $this->Flash->success(__('The apartamento has been deleted.'));
        } else {
            $this->Flash->error(__('The apartamento could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
