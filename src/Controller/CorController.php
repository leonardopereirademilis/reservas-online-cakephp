<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Cor Controller
 *
 * @property \App\Model\Table\CorTable $Cor
 */
class CorController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $cor = $this->paginate($this->Cor);

        $this->set(compact('cor'));
        $this->set('_serialize', ['cor']);
    }

    /**
     * View method
     *
     * @param string|null $id Cor id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cor = $this->Cor->get($id, [
            'contain' => ['Recurso']
        ]);

        $this->set('cor', $cor);
        $this->set('_serialize', ['cor']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cor = $this->Cor->newEntity();
        if ($this->request->is('post')) {
            $cor = $this->Cor->patchEntity($cor, $this->request->data);
            if ($this->Cor->save($cor)) {
                $this->Flash->success(__('The cor has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The cor could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('cor'));
        $this->set('_serialize', ['cor']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cor id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cor = $this->Cor->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cor = $this->Cor->patchEntity($cor, $this->request->data);
            if ($this->Cor->save($cor)) {
                $this->Flash->success(__('The cor has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The cor could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('cor'));
        $this->set('_serialize', ['cor']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cor id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cor = $this->Cor->get($id);
        if ($this->Cor->delete($cor)) {
            $this->Flash->success(__('The cor has been deleted.'));
        } else {
            $this->Flash->error(__('The cor could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
