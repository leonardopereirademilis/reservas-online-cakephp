<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UnidadeTempoReserva Controller
 *
 * @property \App\Model\Table\UnidadeTempoReservaTable $UnidadeTempoReserva
 */
class UnidadeTempoReservaController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $unidadeTempoReserva = $this->paginate($this->UnidadeTempoReserva);

        $this->set(compact('unidadeTempoReserva'));
        $this->set('_serialize', ['unidadeTempoReserva']);
    }

    /**
     * View method
     *
     * @param string|null $id Unidade Tempo Reserva id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $unidadeTempoReserva = $this->UnidadeTempoReserva->get($id, [
            'contain' => ['Recurso']
        ]);

        $this->set('unidadeTempoReserva', $unidadeTempoReserva);
        $this->set('_serialize', ['unidadeTempoReserva']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $unidadeTempoReserva = $this->UnidadeTempoReserva->newEntity();
        if ($this->request->is('post')) {
            $unidadeTempoReserva = $this->UnidadeTempoReserva->patchEntity($unidadeTempoReserva, $this->request->data);
            if ($this->UnidadeTempoReserva->save($unidadeTempoReserva)) {
                $this->Flash->success(__('The unidade tempo reserva has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The unidade tempo reserva could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('unidadeTempoReserva'));
        $this->set('_serialize', ['unidadeTempoReserva']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Unidade Tempo Reserva id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $unidadeTempoReserva = $this->UnidadeTempoReserva->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $unidadeTempoReserva = $this->UnidadeTempoReserva->patchEntity($unidadeTempoReserva, $this->request->data);
            if ($this->UnidadeTempoReserva->save($unidadeTempoReserva)) {
                $this->Flash->success(__('The unidade tempo reserva has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The unidade tempo reserva could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('unidadeTempoReserva'));
        $this->set('_serialize', ['unidadeTempoReserva']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Unidade Tempo Reserva id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $unidadeTempoReserva = $this->UnidadeTempoReserva->get($id);
        if ($this->UnidadeTempoReserva->delete($unidadeTempoReserva)) {
            $this->Flash->success(__('The unidade tempo reserva has been deleted.'));
        } else {
            $this->Flash->error(__('The unidade tempo reserva could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
