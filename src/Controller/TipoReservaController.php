<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TipoReserva Controller
 *
 * @property \App\Model\Table\TipoReservaTable $TipoReserva
 */
class TipoReservaController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $tipoReserva = $this->paginate($this->TipoReserva);

        $this->set(compact('tipoReserva'));
        $this->set('_serialize', ['tipoReserva']);
    }

    /**
     * View method
     *
     * @param string|null $id Tipo Reserva id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tipoReserva = $this->TipoReserva->get($id, [
            'contain' => ['Recurso']
        ]);

        $this->set('tipoReserva', $tipoReserva);
        $this->set('_serialize', ['tipoReserva']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tipoReserva = $this->TipoReserva->newEntity();
        if ($this->request->is('post')) {
            $tipoReserva = $this->TipoReserva->patchEntity($tipoReserva, $this->request->data);
            if ($this->TipoReserva->save($tipoReserva)) {
                $this->Flash->success(__('The tipo reserva has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tipo reserva could not be saved. Please, try again.'));
            }
        }
        $recurso = $this->TipoReserva->Recurso->find('list', ['limit' => 200]);
        $this->set(compact('tipoReserva', 'recurso'));
        $this->set('_serialize', ['tipoReserva']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipo Reserva id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tipoReserva = $this->TipoReserva->get($id, [
            'contain' => ['Recurso']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipoReserva = $this->TipoReserva->patchEntity($tipoReserva, $this->request->data);
            if ($this->TipoReserva->save($tipoReserva)) {
                $this->Flash->success(__('The tipo reserva has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tipo reserva could not be saved. Please, try again.'));
            }
        }
        $recurso = $this->TipoReserva->Recurso->find('list', ['limit' => 200]);
        $this->set(compact('tipoReserva', 'recurso'));
        $this->set('_serialize', ['tipoReserva']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipo Reserva id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tipoReserva = $this->TipoReserva->get($id);
        if ($this->TipoReserva->delete($tipoReserva)) {
            $this->Flash->success(__('The tipo reserva has been deleted.'));
        } else {
            $this->Flash->error(__('The tipo reserva could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
