<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TipoPlano Controller
 *
 * @property \App\Model\Table\TipoPlanoTable $TipoPlano
 */
class TipoPlanoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $tipoPlano = $this->paginate($this->TipoPlano);

        $this->set(compact('tipoPlano'));
        $this->set('_serialize', ['tipoPlano']);
    }

    /**
     * View method
     *
     * @param string|null $id Tipo Plano id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tipoPlano = $this->TipoPlano->get($id, [
            'contain' => ['Plano']
        ]);

        $this->set('tipoPlano', $tipoPlano);
        $this->set('_serialize', ['tipoPlano']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tipoPlano = $this->TipoPlano->newEntity();
        if ($this->request->is('post')) {
            $tipoPlano = $this->TipoPlano->patchEntity($tipoPlano, $this->request->data);
            if ($this->TipoPlano->save($tipoPlano)) {
                $this->Flash->success(__('The tipo plano has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tipo plano could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('tipoPlano'));
        $this->set('_serialize', ['tipoPlano']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipo Plano id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tipoPlano = $this->TipoPlano->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipoPlano = $this->TipoPlano->patchEntity($tipoPlano, $this->request->data);
            if ($this->TipoPlano->save($tipoPlano)) {
                $this->Flash->success(__('The tipo plano has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tipo plano could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('tipoPlano'));
        $this->set('_serialize', ['tipoPlano']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipo Plano id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tipoPlano = $this->TipoPlano->get($id);
        if ($this->TipoPlano->delete($tipoPlano)) {
            $this->Flash->success(__('The tipo plano has been deleted.'));
        } else {
            $this->Flash->error(__('The tipo plano could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
