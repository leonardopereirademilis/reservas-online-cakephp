<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Mensalidade Controller
 *
 * @property \App\Model\Table\MensalidadeTable $Mensalidade
 */
class MensalidadeController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Condominio', 'Plano']
        ];
        $mensalidade = $this->paginate($this->Mensalidade);

        $this->set(compact('mensalidade'));
        $this->set('_serialize', ['mensalidade']);
    }

    /**
     * View method
     *
     * @param string|null $id Mensalidade id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $mensalidade = $this->Mensalidade->get($id, [
            'contain' => ['Condominio', 'Plano', 'Boleto']
        ]);

        $this->set('mensalidade', $mensalidade);
        $this->set('_serialize', ['mensalidade']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $mensalidade = $this->Mensalidade->newEntity();
        if ($this->request->is('post')) {
            $mensalidade = $this->Mensalidade->patchEntity($mensalidade, $this->request->data);
            if ($this->Mensalidade->save($mensalidade)) {
                $this->Flash->success(__('The mensalidade has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The mensalidade could not be saved. Please, try again.'));
            }
        }
        $condominio = $this->Mensalidade->Condominio->find('list', ['limit' => 200]);
        $plano = $this->Mensalidade->Plano->find('list', ['limit' => 200]);
        $this->set(compact('mensalidade', 'condominio', 'plano'));
        $this->set('_serialize', ['mensalidade']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Mensalidade id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $mensalidade = $this->Mensalidade->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mensalidade = $this->Mensalidade->patchEntity($mensalidade, $this->request->data);
            if ($this->Mensalidade->save($mensalidade)) {
                $this->Flash->success(__('The mensalidade has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The mensalidade could not be saved. Please, try again.'));
            }
        }
        $condominio = $this->Mensalidade->Condominio->find('list', ['limit' => 200]);
        $plano = $this->Mensalidade->Plano->find('list', ['limit' => 200]);
        $this->set(compact('mensalidade', 'condominio', 'plano'));
        $this->set('_serialize', ['mensalidade']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Mensalidade id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mensalidade = $this->Mensalidade->get($id);
        if ($this->Mensalidade->delete($mensalidade)) {
            $this->Flash->success(__('The mensalidade has been deleted.'));
        } else {
            $this->Flash->error(__('The mensalidade could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
