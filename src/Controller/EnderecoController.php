<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Endereco Controller
 *
 * @property \App\Model\Table\EnderecoTable $Endereco
 */
class EnderecoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Cidade']
        ];
        $endereco = $this->paginate($this->Endereco);

        $this->set(compact('endereco'));
        $this->set('_serialize', ['endereco']);
    }

    /**
     * View method
     *
     * @param string|null $id Endereco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $endereco = $this->Endereco->get($id, [
            'contain' => ['Cidade', 'Condominio']
        ]);

        $this->set('endereco', $endereco);
        $this->set('_serialize', ['endereco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $endereco = $this->Endereco->newEntity();
        if ($this->request->is('post')) {
            $endereco = $this->Endereco->patchEntity($endereco, $this->request->data);
            if ($this->Endereco->save($endereco)) {
                $this->Flash->success(__('The endereco has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The endereco could not be saved. Please, try again.'));
            }
        }
        $cidade = $this->Endereco->Cidade->find('list', ['limit' => 200]);
        $this->set(compact('endereco', 'cidade'));
        $this->set('_serialize', ['endereco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Endereco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $endereco = $this->Endereco->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $endereco = $this->Endereco->patchEntity($endereco, $this->request->data);
            if ($this->Endereco->save($endereco)) {
                $this->Flash->success(__('The endereco has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The endereco could not be saved. Please, try again.'));
            }
        }
        $cidade = $this->Endereco->Cidade->find('list', ['limit' => 200]);
        $this->set(compact('endereco', 'cidade'));
        $this->set('_serialize', ['endereco']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Endereco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $endereco = $this->Endereco->get($id);
        if ($this->Endereco->delete($endereco)) {
            $this->Flash->success(__('The endereco has been deleted.'));
        } else {
            $this->Flash->error(__('The endereco could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
