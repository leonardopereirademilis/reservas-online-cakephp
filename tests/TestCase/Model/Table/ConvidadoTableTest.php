<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConvidadoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConvidadoTable Test Case
 */
class ConvidadoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConvidadoTable
     */
    public $Convidado;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.convidado',
        'app.reserva'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Convidado') ? [] : ['className' => 'App\Model\Table\ConvidadoTable'];
        $this->Convidado = TableRegistry::get('Convidado', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Convidado);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
