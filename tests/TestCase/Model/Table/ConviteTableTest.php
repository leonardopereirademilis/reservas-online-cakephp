<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConviteTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConviteTable Test Case
 */
class ConviteTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConviteTable
     */
    public $Convite;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.convite',
        'app.apartamento',
        'app.condominio',
        'app.endereco',
        'app.plano',
        'app.mensalidade',
        'app.recurso',
        'app.condominio_convite',
        'app.users',
        'app.condominio_users',
        'app.reserva'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Convite') ? [] : ['className' => 'App\Model\Table\ConviteTable'];
        $this->Convite = TableRegistry::get('Convite', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Convite);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
