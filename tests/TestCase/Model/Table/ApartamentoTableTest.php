<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ApartamentoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ApartamentoTable Test Case
 */
class ApartamentoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ApartamentoTable
     */
    public $Apartamento;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.apartamento',
        'app.condominio',
        'app.convite',
        'app.reserva'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Apartamento') ? [] : ['className' => 'App\Model\Table\ApartamentoTable'];
        $this->Apartamento = TableRegistry::get('Apartamento', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Apartamento);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
