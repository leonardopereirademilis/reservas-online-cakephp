<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RecursoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RecursoTable Test Case
 */
class RecursoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RecursoTable
     */
    public $Recurso;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.recurso',
        'app.condominio',
        'app.endereco',
        'app.cidade',
        'app.estado',
        'app.pais',
        'app.plano',
        'app.tipo_plano',
        'app.users',
        'app.mensalidade',
        'app.boleto',
        'app.apartamento',
        'app.convite',
        'app.condominio_convite',
        'app.reserva',
        'app.condominio_users',
        'app.unidade_tempo_reserva',
        'app.cor',
        'app.imagem',
        'app.indisponibilidade',
        'app.tipo_reserva',
        'app.recurso_tipo_reserva'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Recurso') ? [] : ['className' => 'App\Model\Table\RecursoTable'];
        $this->Recurso = TableRegistry::get('Recurso', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Recurso);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
