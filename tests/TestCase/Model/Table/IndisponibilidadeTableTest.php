<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IndisponibilidadeTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IndisponibilidadeTable Test Case
 */
class IndisponibilidadeTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\IndisponibilidadeTable
     */
    public $Indisponibilidade;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.indisponibilidade',
        'app.recurso'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Indisponibilidade') ? [] : ['className' => 'App\Model\Table\IndisponibilidadeTable'];
        $this->Indisponibilidade = TableRegistry::get('Indisponibilidade', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Indisponibilidade);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
