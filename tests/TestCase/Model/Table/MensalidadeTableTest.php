<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MensalidadeTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MensalidadeTable Test Case
 */
class MensalidadeTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MensalidadeTable
     */
    public $Mensalidade;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.mensalidade',
        'app.condominio',
        'app.endereco',
        'app.cidade',
        'app.estado',
        'app.pais',
        'app.plano',
        'app.apartamento',
        'app.convite',
        'app.users',
        'app.condominio_convite',
        'app.reserva',
        'app.recurso',
        'app.condominio_users',
        'app.boleto'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Mensalidade') ? [] : ['className' => 'App\Model\Table\MensalidadeTable'];
        $this->Mensalidade = TableRegistry::get('Mensalidade', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Mensalidade);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
