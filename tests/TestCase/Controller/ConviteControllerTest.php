<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ConviteController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ConviteController Test Case
 */
class ConviteControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.convite',
        'app.apartamento',
        'app.condominio',
        'app.endereco',
        'app.plano',
        'app.mensalidade',
        'app.recurso',
        'app.condominio_convite',
        'app.users',
        'app.condominio_users',
        'app.reserva'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
