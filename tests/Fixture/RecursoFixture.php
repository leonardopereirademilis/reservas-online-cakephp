<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RecursoFixture
 *
 */
class RecursoFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'recurso';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => 20, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'ativo' => ['type' => 'text', 'length' => null, 'null' => false, 'default' => 'b\'1\'', 'collate' => null, 'comment' => '', 'precision' => null],
        'capacidade' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'condominio_id' => ['type' => 'biginteger', 'length' => 20, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'descricao' => ['type' => 'text', 'length' => null, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'exige_confirmacao' => ['type' => 'text', 'length' => null, 'null' => false, 'default' => 'b\'1\'', 'collate' => null, 'comment' => '', 'precision' => null],
        'nome' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'numero_max_reservas' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => '1', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'tempo_reserva' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => '24', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'valor' => ['type' => 'decimal', 'length' => 19, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'unidade_tempo_reserva_id' => ['type' => 'biginteger', 'length' => 20, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'cor_id' => ['type' => 'biginteger', 'length' => 20, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'FK4089DA492BA20382' => ['type' => 'index', 'columns' => ['condominio_id'], 'length' => []],
            'FK4089DA496CC5715A' => ['type' => 'index', 'columns' => ['unidade_tempo_reserva_id'], 'length' => []],
            'FK4089DA496226C752' => ['type' => 'index', 'columns' => ['cor_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'FK4089DA492BA20382' => ['type' => 'foreign', 'columns' => ['condominio_id'], 'references' => ['condominio', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'FK4089DA496226C752' => ['type' => 'foreign', 'columns' => ['cor_id'], 'references' => ['cor', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'FK4089DA496CC5715A' => ['type' => 'foreign', 'columns' => ['unidade_tempo_reserva_id'], 'references' => ['unidade_tempo_reserva', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'ativo' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'capacidade' => 1,
            'condominio_id' => 1,
            'descricao' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'exige_confirmacao' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'nome' => 'Lorem ipsum dolor sit amet',
            'numero_max_reservas' => 1,
            'tempo_reserva' => 1,
            'valor' => 1.5,
            'unidade_tempo_reserva_id' => 1,
            'cor_id' => 1
        ],
    ];
}
