<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CondominioFixture
 *
 */
class CondominioFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'condominio';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => 20, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'endereco_id' => ['type' => 'biginteger', 'length' => 20, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'nome' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'plano_id' => ['type' => 'biginteger', 'length' => 20, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'FKD25EA18B1B24BCC2' => ['type' => 'index', 'columns' => ['endereco_id'], 'length' => []],
            'FKD25EA18BFFD82552' => ['type' => 'index', 'columns' => ['plano_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'plano_id' => ['type' => 'unique', 'columns' => ['plano_id'], 'length' => []],
            'FKD25EA18B1B24BCC2' => ['type' => 'foreign', 'columns' => ['endereco_id'], 'references' => ['endereco', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'FKD25EA18BFFD82552' => ['type' => 'foreign', 'columns' => ['plano_id'], 'references' => ['plano', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'endereco_id' => 1,
            'nome' => 'Lorem ipsum dolor sit amet',
            'plano_id' => 1
        ],
    ];
}
